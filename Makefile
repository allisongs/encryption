CXXFLAGS = -DNDEBUG -g3 -O2 -Wall -Wextra
all:
	g++ $(CXXFLAGS) -c --std=c++11 cryptoppTest.cpp -I./cryptopp700
	g++ $(CSSFLAGS) -c --std=c++11 crypto.cpp -I./cryptopp700
	g++ $(CSSFLAGS) -c --std=c++11 main.cpp -I./cryptopp700
	g++ -o cryptoFlight crypto.o cryptoppTest.o main.o -L cryptopp700 -l:libcryptopp.a

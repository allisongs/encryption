#ifndef CRYPTOPPTEST_H
#define CRYPTOPPTEST_H
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "crypto.h"
#include <iostream>

class cryptoppTest {

public:
  cryptoppTest(std::string, std::string);

private:
    void copyFileToEncrypt(std::string, std::string);
    void test3des(std::string, std::string, int);
    void testaes(std::string, std::string, int);
    void blocktest(std::string, std::string, int);
    void testhashes(std::string, std::string, int);
    void testsalsa(std::string, std::string, int);
    void testchacha(std::string, std::string, int);
    void streamtest(std::string, std::string, int);

    void testRSA(std::string, std::string, int);
//    void testRabin(std::string, std::string, 
//                   RSA::PublicKey, RSA::PrivateKey, int);
    void pubKeyTest(std::string, std::string, int);

    void testSHA(std::string, std::string, int);
    void testmd5(std::string, std::string, int);

    crypto cryptography;
    bool debugme;
};
#endif // CRYPTOPPTEST_H

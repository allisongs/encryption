//#include <osrng.h>
//using CryptoPP::AutoSeededRandomPool;

#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include "cryptoppTest.h"


// ***************************************************************************
// ***************************************************************************
// CONSTRUCTOR and main driver. 
// Testing the library entails using a block cipher
// test, a stream cipher, a public key cipher and a hash
cryptoppTest::cryptoppTest(std::string inFilename, 
                           std::string outFilename) {
    int n = 8;  // number of runs to get average
    blocktest(inFilename, outFilename, n);
    streamtest(inFilename, outFilename, n);
    pubKeyTest(inFilename, outFilename, n);
    testhashes(inFilename, outFilename, n);
}


// **************************************************************************
// Helper function
void cryptoppTest::copyFileToEncrypt(std::string fileToCopy, 
                       std::string copyToFile) {

    std::fstream src;
    src.open(fileToCopy.c_str(), std::fstream::in);

    std::fstream dst;
    dst.open(copyToFile.c_str(), std::fstream::out | std::ios::trunc);

    bool filesopenedok = true;
    if (!src.is_open()) {
       std::cout << "Unable to open src file for des encrypt.\n";
       filesopenedok = false; 
    }
    if (!dst.is_open()) {
       std::cout << "Unable to open dst file for des encrypt.\n";
       filesopenedok = false; 
    }
    if (filesopenedok) {
       char ch;
       while (src) {
          src.get(ch);
          if (!src.eof()) {
             dst.put(ch);
          }
       }

       src.close();
       dst.close();
   }
}

// ***************************************************************************
// BLOCK TEST
void cryptoppTest::blocktest(std::string inFilename, 
                             std::string outFilename, int n) {
    

    std::string desout = "./intermediateFiles/" + outFilename + "DES";
    //copy over inFilename to "inFilename"DES
    std::string desin = "./intermediateFiles/" + inFilename + "DES";
    copyFileToEncrypt(inFilename, desin);
    //now do the test
    test3des(desin, desout, n);

    std::string aesout = "./intermediateFiles/" + outFilename + "AES";
    //copy over inFilename to "inFilename"AES
    std::string aesin = "./intermediateFiles/" + inFilename + "AES";
    copyFileToEncrypt(inFilename, aesin);
    //now do the test
    testaes(aesin, aesout, n);
 
}

// BLOCK TEST: 3DES
void cryptoppTest::test3des(std::string inFilename, 
                            std::string outFilename,
                            int n) {

    clock_t enStartTime, enStopTime, decStartTime, decStopTime;
    double enTotalTime = 0; double decTotalTime = 0;

    for (int i = 0; i < n; i++) {
        // encrypt
        enStartTime = clock();
        cryptography.test = false;
        cryptography.TripleDESCBC(inFilename, outFilename);
        enStopTime = clock();
        enTotalTime += (enStopTime - enStartTime);

        // decrypt
        decStartTime = clock();
        cryptography.UnTripleDESCBC(inFilename, outFilename);
        decStopTime = clock();
        decTotalTime += (decStopTime - decStartTime);
    }
    double enAvgTime = (enTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "3DES EDE3 avg encrypt time(ms): " << enAvgTime << "\n";
    double decAvgTime = (decTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "3DES EDE3 avg decrypt time(ms): " << decAvgTime << "\n";
}


// BLOCK TEST: AES
void cryptoppTest::testaes(std::string inFilename, 
                           std::string outFilename,
                           int n) {

    clock_t enStartTime, enStopTime, decStartTime, decStopTime;
    double enTotalTime = 0; double decTotalTime = 0;

    for (int i = 0; i < n; i++) {
        enStartTime = clock();
        cryptography.AES128CBC(inFilename, outFilename);
        enStopTime = clock();
        enTotalTime += (enStopTime - enStartTime);

        decStartTime = clock();
        cryptography.UnAES128CBC(inFilename, outFilename);
        decStopTime = clock();
        decTotalTime += (decStopTime - decStartTime);
    }
    double enAvgTime = (enTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "AES avg encrypt time(ms): " << enAvgTime << "\n";
    double decAvgTime = (decTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "AES avg decrypt time(ms): " << decAvgTime << "\n";
}


// ***************************************************************************
// STREAM TEST
void cryptoppTest::streamtest(std::string inFilename, 
                              std::string outFilename, int n) {
    std::string salsaOut = "./intermediateFiles/" + outFilename + "SALSA";
    //copy over inFilename to "inFilename"SALSA
    std::string salsaIn = "./intermediateFiles/" + inFilename + "SALSA";
    copyFileToEncrypt(inFilename, salsaIn);
    testsalsa(salsaIn, salsaOut, n);

    std::string chachaOut = "./intermediateFiles/" + outFilename + "CHACHA";
    //copy over inFilename to "inFilename"SALSA
    std::string chachaIn = "./intermediateFiles/" + inFilename + "CHACHA";
    copyFileToEncrypt(inFilename, chachaIn);
    testchacha(chachaIn, chachaOut, n);
}

// STREAM TEST: SALSA
void cryptoppTest::testsalsa(std::string inFilename,
	                          std::string outFilename,
	                          int n) {
	clock_t enStartTime, enStopTime, decStartTime, decStopTime;
	double enTotalTime = 0; double decTotalTime = 0;
	for (int i = 0; i < n; i++) {
		enStartTime = clock();
		cryptography.Salsa(inFilename, outFilename);
		enStopTime = clock();
		enTotalTime += (enStopTime - enStartTime);
		decStartTime = clock();
		cryptography.UnSalsa(inFilename, outFilename);
		decStopTime = clock();
		decTotalTime += (decStopTime - decStartTime);
	}
	double enAvgTime = (enTotalTime / ((double)(CLOCKS_PER_SEC)
		/ (double)1000))
		/ (double)n;
	std::cout << "Salsa20 avg encrypt time(ms): " << enAvgTime << "\n";
	double decAvgTime = (decTotalTime / ((double)(CLOCKS_PER_SEC)
		/ (double)1000))
		/ (double)n;
	std::cout << "Salsa20 avg decrypt time(ms): " << decAvgTime << "\n";
}


void cryptoppTest::testchacha(std::string inFilename, 
                           std::string outFilename,
                           int n) {

    clock_t enStartTime, enStopTime, decStartTime, decStopTime;
    double enTotalTime = 0; double decTotalTime = 0;
    for (int i = 0; i < n; i++) {
        enStartTime = clock();
        cryptography.chacha(inFilename, outFilename);
        enStopTime = clock();
        enTotalTime += (enStopTime - enStartTime);
        decStartTime = clock();
        cryptography.Unchacha(inFilename, outFilename);
        decStopTime = clock();
        decTotalTime += (decStopTime - decStartTime);
    }
    double enAvgTime = (enTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "ChaCha20 avg encrypt time(ms): " << enAvgTime << "\n";
    double decAvgTime = (decTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "ChaCha20 avg decrypt time(ms): " << decAvgTime << "\n";
}

// ***************************************************************************
// ASYMMETRIC KEY
void cryptoppTest::pubKeyTest(std::string inFilename,
                              std::string outFilename, int n) {
    std::string rsaout = "./intermediateFiles/" + outFilename + "RSA";
    std::string rsain = "./intermediateFiles/" + inFilename + "RSA";
    copyFileToEncrypt(inFilename, rsain);
    testRSA(rsain, rsaout, n);
}

//// ASYMMETRIC KEY: RSA
void cryptoppTest::testRSA(std::string inFilename, 
                           std::string outFilename,
                           int n) {

    clock_t enStartTime, enStopTime, decStartTime, decStopTime;
    double enTotalTime = 0; double decTotalTime = 0;
    for (int i = 0; i < n; i++) {
        enStartTime = clock();
        cryptography.RSAencrypt(inFilename, outFilename);
        enStopTime = clock();
        enTotalTime += (enStopTime - enStartTime);
        decStartTime = clock();
        cryptography.UnRSAencrypt(inFilename, outFilename);
        decStopTime = clock();
        decTotalTime += (decStopTime - decStartTime);
    }
    double enAvgTime = (enTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "RSA avg encrypt time(ms): " << enAvgTime << "\n";
    double decAvgTime = (decTotalTime/((double)(CLOCKS_PER_SEC)
                     /(double)1000)) 
                     / (double)n;
    std::cout << "RSA avg decrypt time(ms): " << decAvgTime << "\n";
}



// ***************************************************************************
// TEST HASH
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

void cryptoppTest::testhashes(std::string fileToEncrypt, 
	                          std::string EncryptedFile, int n) {
	testSHA(fileToEncrypt, EncryptedFile, n);
	testmd5(fileToEncrypt, EncryptedFile, n);
}

void cryptoppTest::testSHA(std::string fileToEncrypt, 
	                       std::string EncryptedFile, int n) {
	clock_t enStartTime, enStopTime;
	double enTotalTime = 0; 
	
	for (int i = 0; i < n; i++) {
		enStartTime = clock();
		cryptography.SHA(fileToEncrypt, EncryptedFile);
		enStopTime = clock();
		enTotalTime += (enStopTime - enStartTime);
	}
	
	double enAvgTime = (enTotalTime / ((double)(CLOCKS_PER_SEC)
		/ (double)1000))
		/ (double)n;
	std::cout << "SHA average hash time(ms): " << enAvgTime << "\n";
}

void cryptoppTest::testmd5(std::string fileToEncrypt,
	                        std::string EncryptedFile, int n) {
	cryptography.md5(fileToEncrypt, EncryptedFile);  // TODO
	clock_t enStartTime, enStopTime;
	double enTotalTime = 0; 

	for (int i = 0; i < n; i++) {
		enStartTime = clock();
		cryptography.md5(fileToEncrypt, EncryptedFile);
		enStopTime = clock();
		enTotalTime += (enStopTime - enStartTime);
	}

	double enAvgTime = (enTotalTime / ((double)(CLOCKS_PER_SEC)
		/ (double)1000))
		/ (double)n;
	std::cout << "MD5 average hash time(ms): " << enAvgTime << "\n";

}


//
// main.cpp
// CryptoFlight
//
// Created by Allison Gibson 11/26/2016
// Updated 09/2018
// Copyright 2016 Allison Gibson. All rights reserved
//
#include <iostream>
#include "cryptoppTest.h"


int main(int argc, const char* argv[]) {
    if (argc == 3) {
        std::string infile = argv[1];
        std::string outfile = argv[2];
        std::cout << "calling crytpoppTest\n";
        cryptoppTest(infile, outfile);
    }
    return 0;
}

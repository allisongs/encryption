#include <osrng.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include "cryptoppTest.h"

#include "cryptlib.h"
#include "hex.h"
#include "files.h"
#include "filters.h"

#include "des.h"
#include "aes.h"
#include "salsa.h"
#include "chacha.h"
#include "rsa.h"

#include "integer.h"
#include "modes.h"
#include "secblock.h"

#include "md5.h"
#include "base64.h"

// ***************************************************************************
// ***************************************************************************
// CONSTRUCTOR 
crypto::crypto() {
    // randomly generating the key and the iv 
    myDESkeys.key.New(CryptoPP::DES_EDE3::DEFAULT_KEYLENGTH); 
    myDESkeys.iv= new byte[CryptoPP::DES_EDE3::BLOCKSIZE];
    initialization(myDESkeys.key, myDESkeys.iv);

    myAESkeys.key.New(CryptoPP::AES::DEFAULT_KEYLENGTH); 
    myAESkeys.iv= new byte[CryptoPP::AES::BLOCKSIZE];
    initialization(myAESkeys.key, myAESkeys.iv);

    mySALSAkeys.key.New(CryptoPP::Salsa20::DEFAULT_KEYLENGTH);
    mySALSAkeys.iv = new byte[CryptoPP::Salsa20::IV_LENGTH];
    initialization(mySALSAkeys.key, mySALSAkeys.iv);

    myCHACHAkeys.key.New(CryptoPP::ChaCha20::DEFAULT_KEYLENGTH);
    myCHACHAkeys.iv = new byte[CryptoPP::ChaCha20::IV_LENGTH];
    initialization(myCHACHAkeys.key, myCHACHAkeys.iv);

    AutoSeededRandomPool randomNumberGen;
    unsigned int keySize = 3072;
    myRSAkeys.params.GenerateRandomWithKeySize(
       randomNumberGen, keySize);

    myRSAkeys.privkey.AssignFrom(myRSAkeys.params);
    CryptoPP::Base64Encoder privkeysink(new CryptoPP::FileSink("RSAprivkey.txt"));
    myRSAkeys.privkey.DEREncode(privkeysink);
    privkeysink.MessageEnd();

    myRSAkeys.pubkey.AssignFrom(myRSAkeys.params);
    CryptoPP::Base64Encoder pubkeysink(new CryptoPP::FileSink("RSApubkey.txt"));
    myRSAkeys.pubkey.DEREncode(pubkeysink);
    pubkeysink.MessageEnd();
}

void crypto::initialization(CryptoPP::SecByteBlock& key, byte* iv) {
    prng.GenerateBlock(key, key.size());
    prng.GenerateBlock(iv, sizeof(iv));

    encoded.clear();
    CryptoPP::StringSource(key, key.size(), true, 
         new CryptoPP::HexEncoder( new CryptoPP::StringSink(encoded) ) );

    encoded.clear();
    CryptoPP::StringSource(iv, sizeof(iv), true, 
        new CryptoPP::HexEncoder( new CryptoPP::StringSink(encoded) ) );
}

crypto::~crypto() {
    myDESkeys.key.CleanNew(CryptoPP::DES_EDE3::DEFAULT_KEYLENGTH);
    delete [] myDESkeys.iv;
    myAESkeys.key.CleanNew(CryptoPP::AES::DEFAULT_KEYLENGTH);
    delete [] myAESkeys.iv;
    mySALSAkeys.key.CleanNew(CryptoPP::Salsa20::DEFAULT_KEYLENGTH);
    delete [] mySALSAkeys.iv;
    myCHACHAkeys.key.CleanNew(CryptoPP::ChaCha20::DEFAULT_KEYLENGTH);
    delete [] myCHACHAkeys.iv;
}

void crypto::TripleDESCBC(std::string inFilename, 
                          std::string outFilename) {
    try {
        // create encryption object with 3DES encrypt/decrypt/encrypt
        // 3 keys.

        CryptoPP::CBC_Mode< CryptoPP::DES_EDE3 >::Encryption encryp;
        encryp.SetKeyWithIV(myDESkeys.key, myDESkeys.key.size(), myDESkeys.iv);
        // note that StreamTransformationFilter adds necessary padding
        CryptoPP::FileSource(
           inFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              encryp,
              new CryptoPP::FileSink(outFilename.c_str()),
              CryptoPP::StreamTransformationFilter::ZEROS_PADDING 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}


void crypto::UnTripleDESCBC(std::string inFilename, 
                            std::string outFilename) {
    try {
        // create decryption object with 3DES encrypt/decrypt/encrypt
        // 3 keys.
        CryptoPP::CBC_Mode< CryptoPP::DES_EDE3 >::Decryption decryp;
        decryp.SetKeyWithIV(myDESkeys.key, myDESkeys.key.size(), myDESkeys.iv);
        // note that StreamTransformationFilter removes padding
        CryptoPP::FileSource(
           outFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              decryp,
              new CryptoPP::FileSink(
                 inFilename.c_str()),
              CryptoPP::StreamTransformationFilter::ZEROS_PADDING 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

void crypto::AES128CBC(std::string inFilename, 
                       std::string outFilename) {
    try {
        // create encryption object with AES. The default key length in 
        // cryptopp is 16 bytes or 128 bits.
        CryptoPP::CBC_Mode< CryptoPP::AES >::Encryption encryp;
        encryp.SetKeyWithIV(myAESkeys.key, myAESkeys.key.size(), myAESkeys.iv);
        // note that StreamTransformationFilter adds necessary padding
        CryptoPP::FileSource(
           inFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              encryp,
              new CryptoPP::FileSink(outFilename.c_str()) 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

void crypto::UnAES128CBC(std::string inFilename, 
                         std::string outFilename) {
    try {
        // create decryption object with AES 128
        CryptoPP::CBC_Mode< CryptoPP::AES >::Decryption decryp;
        decryp.SetKeyWithIV(myAESkeys.key, myAESkeys.key.size(), myAESkeys.iv);
        // note that StreamTransformationFilter removes padding
        CryptoPP::FileSource(
           outFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              decryp,
              new CryptoPP::FileSink(inFilename.c_str()) 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}


void crypto::Salsa(std::string inFilename, 
                     std::string outFilename) {
    try {
        CryptoPP::Salsa20::Encryption encryp;
        encryp.SetKeyWithIV(mySALSAkeys.key, mySALSAkeys.key.size(), mySALSAkeys.iv);
        // note that StreamTransformationFilter adds necessary padding
        CryptoPP::FileSource(
           inFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              encryp,
              new CryptoPP::FileSink(outFilename.c_str()) 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

  void crypto::UnSalsa(std::string inFilename,
                         std::string outFilename) {
    try {
        CryptoPP::Salsa20::Decryption decryp;
        decryp.SetKeyWithIV(mySALSAkeys.key, mySALSAkeys.key.size(), mySALSAkeys.iv);
        // note that StreamTransformationFilter removes padding
        CryptoPP::FileSource(
           outFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              decryp,
              new CryptoPP::FileSink(inFilename.c_str()) 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

// STREAM TEST: CHACHA
void crypto::chacha(std::string inFilename, std::string outFilename) {
    try {
        CryptoPP::ChaCha20::Encryption encryp;
        encryp.SetKeyWithIV(myCHACHAkeys.key, myCHACHAkeys.key.size(), myCHACHAkeys.iv);
        // note that StreamTransformationFilter adds necessary padding
        CryptoPP::FileSource(
           inFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              encryp,
              new CryptoPP::FileSink(outFilename.c_str()) 
           ) 
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

void crypto::Unchacha(std::string inFilename, std::string outFilename) {
    try {
        // create decryption object with AES 128
        CryptoPP::ChaCha20::Decryption decryp;
        decryp.SetKeyWithIV(myCHACHAkeys.key, myCHACHAkeys.key.size(), myCHACHAkeys.iv);
        // note that StreamTransformationFilter removes padding
        CryptoPP::FileSource(
           outFilename.c_str(), 
           true,
           new CryptoPP::StreamTransformationFilter(
              decryp,
              new CryptoPP::FileSink(inFilename.c_str()) 
           ) 
       );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

void crypto::RSAencrypt(std::string inFilename, 
                       std::string outFilename) {
    try {
        CryptoPP::RSAES_OAEP_SHA_Encryptor encryp(myRSAkeys.pubkey);
        CryptoPP::FileSource fs1(
           inFilename.c_str(), 
           true,
           new CryptoPP::PK_EncryptorFilter(
              prng, 
              encryp,
              new CryptoPP::HexEncoder(
                 new CryptoPP::FileSink(outFilename.c_str() )
              )
           )
        );
    } catch (const CryptoPP::Exception& error) {
        std::cerr << error.what() << "\n";
        exit(1);
    }
}

  void crypto::UnRSAencrypt(std::string inFilename, 
                           std::string outFilename) {
    try {
        // create encyption object
        CryptoPP::RSAES_OAEP_SHA_Decryptor decryp(myRSAkeys.privkey);
        CryptoPP::FileSource fs1(
           outFilename.c_str(), 
           true,
           new CryptoPP::HexDecoder(
              new CryptoPP::PK_DecryptorFilter(
                 prng, 
                 decryp,
                 new CryptoPP::FileSink( inFilename.c_str() )
              )
           )
        );
    } catch (const CryptoPP::Exception& e) {
        std::cerr << e.what() << "\n";
        exit(1);
    }
}

//void crypto::Rabin(std::string inFilename, 
//                           std::string outFilename, 
//                       RSA::PublicKey) {
//    try {
//
//    } catch (const CryptoPP::Exception& e) {
//        std::cerr << e.what() << "\n";
//        exit(1);
//    }
//}
//
//  void crypto::UnRabin(std::string inFilename, 
//                           std::string outFilename, 
//                       RSA::PrivateKey) {
//
//    try {
//
//    } catch (const CryptoPP::Exception& e) {
//        std::cerr << e.what() << "\n";
//        exit(1);
//    }
//}
//
//void crypto::testRabin(std::string inFilename, 
//            std::string outFilename,
//            RSA::PublicKey pubKey, RSA::PrivateKey privKey, int n) {
//    outFilename = outFilename + "rabin";
//    clock_t enStartTime, enStopTime, decStartTime, decStopTime;
//    double enTotalTime = 0; double decTotalTime = 0;
//    for (int i = 0; i < n; i++) {
//        enStartTime = clock();
//        Rabin(inFilename, outFilename, pubKey);
//        enStopTime = clock();
//        enTotalTime += (enStopTime - enStartTime);
//
//        decStartTime = clock();
//        Rabin(inFilename, outFilename, privKey);
//        decStopTime = clock();
//        decTotalTime += (decStopTime - decStartTime);
//    }
//    double enAvgTime = (enTotalTime/((double)(CLOCKS_PER_SEC)
//                     /(double)1000)) 
//                     / (double)n;
//    std::cout << "Rabin avg encrypt time(ms): " << enAvgTime << "\n";
//    double decAvgTime = (decTotalTime/((double)(CLOCKS_PER_SEC)
//                     /(double)1000)) 
//                     / (double)n;
//    std::cout << "Rabin avg decrypt time(ms): " << decAvgTime << "\n";
//}


// ***************************************************************************
// TEST HASH
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1


void crypto::SHA(std::string fileToEncrypt, std::string EncryptedFile) {
	std::string encodedDigest;
	encodedDigest.clear();
	CryptoPP::SHA256 hashSha;
	std::string digest;
	CryptoPP::FileSource f(fileToEncrypt.c_str(),
		new CryptoPP::HashFilter(hashSha, 
		new CryptoPP::FileSink(EncryptedFile.c_str())));
}

void crypto::md5(std::string fileToEncrypt, std::string EncryptedFile) {
	std::string encodedDigest;
	encodedDigest.clear();
	CryptoPP::Weak1::MD5 hashMD5;
	std::string digest;
	CryptoPP::FileSource f(fileToEncrypt.c_str(),
		new CryptoPP::HashFilter(hashMD5,
			new CryptoPP::FileSink(EncryptedFile.c_str())));
}



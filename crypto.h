//#pragma comment(lib, "cryptlib.lib")
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#ifndef CRYPTO_H
#define CRYPTO_H 
#include <iostream>

#include <osrng.h>
using CryptoPP::AutoSeededRandomPool;
#include "cryptlib.h"
#include "secblock.h"
#include "filters.h"
#include "rsa.h"
using CryptoPP::RSA;

typedef unsigned char byte;

class crypto {

public:
  crypto();
  ~crypto();

  void TripleDESCBC(std::string, std::string);
  void UnTripleDESCBC(std::string, std::string);
  void AES128CBC(std::string, std::string);
  void UnAES128CBC(std::string, std::string);
  void Salsa(std::string, std::string);
  void UnSalsa(std::string, std::string);
  void chacha(std::string, std::string);
  void Unchacha(std::string, std::string);

  void RSAencrypt(std::string, std::string);
  void UnRSAencrypt(std::string, std::string);
  
//  void Rabin(std::string, std::string, RSA::PublicKey);
//  void UnRabin(std::string, std::string, RSA::PrivateKey);
  
  void SHA(std::string, std::string);
  void md5(std::string, std::string);

  bool test;
private:
    struct DESprivate{
       byte* iv;
       CryptoPP::SecByteBlock key;
    };
    DESprivate myDESkeys;
    struct AESprivate{
       byte* iv;
       CryptoPP::SecByteBlock key;
    };
    AESprivate myAESkeys;
    struct Salsaprivate{
       byte* iv;
       CryptoPP::SecByteBlock key;
    };
    Salsaprivate mySALSAkeys;
    struct ChaChaprivate{
       byte* iv;
       CryptoPP::SecByteBlock key;
    };
    ChaChaprivate myCHACHAkeys;

    struct RSAprivate{
       CryptoPP::InvertibleRSAFunction params;
       CryptoPP::RSA::PrivateKey privkey;
       CryptoPP::RSA::PublicKey pubkey;
    };
    RSAprivate myRSAkeys;

    void initialization(CryptoPP::SecByteBlock&, byte*);
    AutoSeededRandomPool prng;
    std::string plain, cipher, encoded, recovered;

};
#endif  // CRYPTO_H

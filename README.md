# CryptoPP File Encryption

Sometimes using encryption libraries is confusing. 
I've started this project to make things easy.
Here are some examples of writing encryption code using the cryptoPP library.
Each encryption and decryption is done to and from a file.

The code as is runs on Ubuntu. It uses cryptoPP version 7.0. So that is included explicitly.
In the future, more library versions, different encryption libraries, different I/O and platforms may be added.
A better API could also be included.

## Getting Started

The code as is is tested on Ubuntu 18.04.01.
I have included a copy of CryptoPP version 7.0 as this is where it was at when I started.
Please note the cryptoPP/License.txt.

### Prerequisites

- Ubuntu 

## Running 

run with this command: "cryptoFlight <FileToEncrypt> <FileToDecrypt>"

### Break down 

For each algorithm 
   -  the program copies over the <FileToEncrypt> and appends
the name of the algorithm onto the end. These files are in the directory
"intermediateFiles". 

   - A timer is set and the encryption algorithm is run
eight times, the average time taken is given as output. 

   - The timer is then set and the decryption is done eight times. The 
average time calculated and output. 

   - For each encryption and decryption, the file is sourced and destined to a file
with the name of the algorithm appended. Files found in /intermediateFiles.
This allows for you to see that the
encryption and decryption are successfully accomplished.

## Authors

* **Allison Gibosn** - 

## License
Note License.txt of cryptoPP700 code.

The rest of the code falls under the MIT license:
MIT License:

Copyright (c) [2018] [Allison Gibson]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
